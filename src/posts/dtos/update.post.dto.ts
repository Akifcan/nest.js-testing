import { IsNotEmpty } from 'class-validator';
export default class UpdatePostDto {
  @IsNotEmpty()
  title: string;
  @IsNotEmpty()
  content: string;
  @IsNotEmpty()
  imageUrl: string;
}
