import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type PostDocument = Post & Document;
@Schema({ timestamps: true })
export class Post {
  @Prop({ required: true, type: String })
  title: string;
  @Prop({ required: true, type: String })
  content: string;
  @Prop({ required: true, type: String })
  imageUrl: string;
}
export const PostSchema = SchemaFactory.createForClass(Post);
