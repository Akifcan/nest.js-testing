import {
  Body,
  Controller,
  Delete,
  Get,
  Inject,
  Param,
  Post,
  Put,
  HttpCode
} from '@nestjs/common';
import CreatePostDto from './dtos/create.post.dto';
import UpdatePostDto from './dtos/update.post.dto';
import { PostsService } from './posts.service';
import { PostDocument } from './entities/post.schema';

@Controller('posts')
export class PostsController {
  @Inject() postsService: PostsService;

  @HttpCode(201)
  @Post()
  async createPost(@Body() post: CreatePostDto): Promise<PostDocument> {
    return await this.postsService.createPost(post);
  }

  @HttpCode(200)
  @Get()
  async listPost(): Promise<Array<PostDocument>> {
    return await this.postsService.listPosts();
  }

  @HttpCode(200)
  @Get(':id')
  async singlePost(@Param() params): Promise<PostDocument> {
    return await this.postsService.singlePost(params.id);
  }

  @HttpCode(202)
  @Delete(':id')
  async deletePost(@Param() params): Promise<{ deletedCount: number }> {
    return await this.postsService.deletePost(params.id);
  }
  @HttpCode(301)
  @Put(':id')
  async updatePost(@Param() params, @Body() post: UpdatePostDto) {
    return await this.postsService.updatePost(params.id, post);
  }
}
