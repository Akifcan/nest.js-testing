import {
  BadRequestException,
  Injectable,
  NotFoundException
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import CreatePostDto from './dtos/create.post.dto';
import UpdatePostDto from './dtos/update.post.dto';
import { Post, PostDocument } from './entities/post.schema';
import { Model, Types } from 'mongoose';

@Injectable()
export class PostsService {
  @InjectModel(Post.name) private postModel: Model<PostDocument>;
  async createPost(post: CreatePostDto): Promise<PostDocument> {
    try {
      const newPost = await this.postModel.create(post);
      return newPost;
    } catch (e) {
      throw new BadRequestException();
    }
  }

  async listPosts(): Promise<Array<PostDocument>> {
    try {
      return await this.postModel.find();
    } catch (e) {
      throw new BadRequestException();
    }
  }

  async singlePost(id: string): Promise<PostDocument> {
    const post = await this.postModel.findOne({
      _id: new Types.ObjectId(id)
    });
    if (post) {
      return post;
    } else {
      throw new NotFoundException();
    }
  }
  async deletePost(id: string): Promise<{ deletedCount: number }> {
    console.log('deleted');
    try {
      return await this.postModel.deleteOne({ _id: new Types.ObjectId(id) });
    } catch (e) {
      throw new BadRequestException();
    }
  }

  async updatePost(id: string, post: UpdatePostDto) {
    try {
      return await this.postModel.updateOne(
        { _id: new Types.ObjectId(id) },
        { $set: post }
      );
    } catch (e) {
      throw new BadRequestException();
    }
  }

  runTest() {
    console.log('run in test demo');
  }
}
