import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { PostsModule } from '../src/posts/posts.module';
import CreatePostDto from '../src/posts/dtos/create.post.dto';
import { AppModule } from '../src/app.module';
import { PostsService } from '../src/posts/posts.service';

describe('Posts', () => {
  let app: INestApplication;
  let postsService: PostsService;

  let existPostId = '6139b90997e7b2297d5db45f';
  let addedPostId = '';
  let notExistPostId = '6139b90997e7b2297d5db45c';

  let validCreatePost: CreatePostDto = {
    title: '{TESTING-DATA}my post title updatedfffffffffffffff test',
    imageUrl: 'assume image url',
    content: 'post content'
  };

  let unvalidCreatePost = {
    imageUrl: 'assume image url',
    content: 'post content'
  };

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [AppModule, PostsModule]
    }).compile();
    app = moduleRef.createNestApplication();
    postsService = moduleRef.get<PostsService>(PostsService);

    await app.init();
  });

  afterAll(async () => {
    if (addedPostId.length > 0) {
      postsService.deletePost(addedPostId);
    }
    await app.close();
  });

  describe('list', () => {
    it('LIST ALL POSTS - should be return 200', async (done) => {
      return request(app.getHttpServer())
        .get('/posts')
        .then((response) => {
          expect(response.statusCode).toBe(200);
          done();
        });
    });
    it('LIST SINGLE POST - should be return 200', async (done) => {
      return request(app.getHttpServer())
        .get(`/posts/${existPostId}`)
        .then((response) => {
          expect(response.statusCode).toBe(200);
          done();
        });
    });
    it('LIST SINGLE POST - should be return 404 when post does not exist', async (done) => {
      return request(app.getHttpServer())
        .get(`/posts/${notExistPostId}`)
        .then((response) => {
          expect(response.statusCode).toBe(404);
          done();
        });
    });
  });

  describe('add', () => {
    it('ADD POST should be return 201 when success', async (done) => {
      return request(app.getHttpServer())
        .post(`/posts`)
        .send(validCreatePost)
        .then((response) => {
          addedPostId = response.body._id;
          expect(response.statusCode).toBe(201);
          done();
        });
    });
    it('ADD POST should be return 400 when fields are unvalid', async (done) => {
      return request(app.getHttpServer())
        .post(`/posts`)
        .send(unvalidCreatePost)
        .then((response) => {
          expect(response.statusCode).toBe(400);
          done();
        });
    });
  });
});
